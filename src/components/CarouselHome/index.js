import React from 'react';
import { Carousel } from  'react-bootstrap';
import b1 from '../../assets/poke1.jpg';
import b2 from '../../assets/poke2.jpg';

export default function CarouselHome(){

    return(
        <>
            <Carousel >
                <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={ b1 }
                    alt="First"
                />
                <Carousel.Caption>
            
                </Carousel.Caption>
                </Carousel.Item>
                
                <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={ b2 }
                    alt="Third "
                />
            
                <Carousel.Caption>
            
                </Carousel.Caption>
                </Carousel.Item>
            
            </Carousel>
        </>
    );
}